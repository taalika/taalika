<?php
/*
 * taalika/index.php
 *
 * Entry point for the application.
 * =============================================================================
 * The MIT License (http://www.opensource.org/licenses/mit-license.php)
 *
 * Copyright (c) 2012 Sanjeev Premi (spremi at ymail.com).
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * =============================================================================
 */


/*
 * Send page headers
 */
header("Expires: Sat, 01 Jan 2000 05:00:00 GMT");
header("Last-Modified: " . gmdate ('D, d M Y H:i:s') . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");


/**
 * Magic phrase used to prevent direct access to any of the scripts
 * included in this application.
 */
define('__TAALIKA__', 1);

/**
 * Define application name.
 */
define('APP_NAME', 'Taalika');

/**
 * Define application version.
 */
define('APP_VERSION', '0.80');

/**
 * Define name of the author.
 */
define('APP_AUTH_NAME', 'Sanjeev Premi');

/**
 * Define email address of the author.
 */
define('APP_AUTH_MAIL', 'spremi@ymail.com');

/**
 * Define base directory for this application.
 */
define('APP_BASE', getcwd());

/**
 * Indicates successful completion of an operation.
 */
define('SUCCESS', true);

/**
 * Indicates unsuccessful completion of an operation.
 */
define('FAILURE', false);

require_once('./include/common.php');

// -----------------------------------------------------------------------------
// Begin
// -----------------------------------------------------------------------------

/*
 * Start the session
 */
session_start();

/*
 * Set flag to continue existing session.
 */
$_SESSION['Remember'] = true ;

/*
 * Check if the application is installed.
 */
if (isInstalled())
	require_once('./include/main.php');
else
	require_once('./install/install.php');

// -----------------------------------------------------------------------------
// End
// -----------------------------------------------------------------------------

/*
 * Destroy the session
 */
if ($_SESSION['Remember'] == false)
	session_destroy();
?>
