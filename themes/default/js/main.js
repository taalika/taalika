/*
 * taalika/themes/default/js/include/main.js
 *
 * Few bells-and-whistles for the user interface
 * =============================================================================
 * The MIT License (http://www.opensource.org/licenses/mit-license.php)
 *
 * Copyright (c) 2012 Sanjeev Premi (spremi at ymail.com).
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * =============================================================================
 */


/**
 * Are we showing the 'About' dialog?
 */
var s_about = false;

/**
 * Position the 'About' box in center of the viewport
 */
function CenterAbout()
{
	  var e_dims = $('pgAbout').getDimensions();
	  var v_dims = document.viewport.getDimensions();

	  var l = (v_dims.width  - e_dims.width) / 2;
	  var t = (v_dims.height - e_dims.height) / 2;

	  $('pgAbout').setStyle({
				top: t + 'px',
				left: l + 'px'
			});
}

/**
 * Bind key to show/ hide the 'About' box
 */
function HandleKeypress(key)
{
	if (s_about) {
		if (key == Event.KEY_ESC) {
			$('pgAbout').hide();
			s_about = false;
		}
	} else {
		if (key == Event.KEY_HOME) {
			$('pgAbout').show();
			s_about = true;
		}
	}
}

/**
 * Handle mouse click in the 'About' box
 */
function HandleClick()
{
	if (s_about == true) {
		$('pgAbout').hide();
		s_about = false;
	}
}

/**
 * Show information specific to an element (On MouseOver)
 */
function ShowInfo(elem)
{
	var idx = '_e_' + elem;

	if (idx in ArrCatalog) {
		$('fileInfo').update(ArrCatalog[idx]);
		$('fileInfo').show();
	}
}

/**
 * Hide information pane (On MouseOut)
 */
function HideInfo()
{
	$('fileInfo').hide();
}


/*
 * Wait for the page to be fully loaded.
 */
Event.observe(window, 'load',
		function() {
			Event.observe(document, 'keypress',
				      function (ev) {
						HandleKeypress(ev.keyCode);
				      });

	/*
	 * Click anywhere on the 'About' box to hide it
	 */
	Event.observe('pgAbout', 'click', HandleClick);

	/*
	 * Center the 'About' box
	 * Do so on each resize event.
	 */
	CenterAbout();
	Event.observe(window, 'resize', CenterAbout);

	/*
	 * Observe mouseover event on listed elements
	 */
	$$('a.elem').invoke('observe', 'mouseover',
				function (ev) {
					ShowInfo(this.innerHTML);

			    });
	/*
	 * Observe mouseout event on listed elements
	 */
	$$('a.elem').invoke('observe', 'mouseout',
				function (ev) {
					HideInfo();
			    });
});
