<?php
/*
 * taalika/themes/default/icons.php
 *
 * Map file extensions to icons.
 * =============================================================================
 * The MIT License (http://www.opensource.org/licenses/mit-license.php)
 *
 * Copyright (c) 2012 Sanjeev Premi (spremi at ymail.com).
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * =============================================================================
 */


/*
 * Prevent direct access
 */
defined ('__TAALIKA__')
or
die(
	'<h2>Taalika</h2>' .
	'<p>Direct access to any of the scripts is not allowed.</p>'
);

$GLOBALS['Icons'] = array(
	/*
	 * Default icon
	 */
	'default'	=> 'default.png',

	/*
	 * Default directory
	 */
	'_DIR_'		=> 'dir.png',
	'_UP_'		=> 'up.png',

	/*
	 * Common document formats
	 */
	'txt'       => 'txt.png',
	'pdf'       => 'pdf.png',
	'doc'       => 'doc.png',
	'dot'       => 'dot.png',

	'xls'       => 'xls.png',
	'xlt'       => 'xlt.png',

	'ppt'       => 'ppt.png',
	'pot'       => 'pot.png',

	'mpp'       => 'mpp.png',

	/*
	 * Common image formats
	 */
	'png'       => 'png.png',
	'jpg'       => 'jpg.png',
	'bmp'       => 'bmp.png',
	'gif'       => 'gif.png',
	'xcf'       => 'xcf.png',

	/*
	 * Common archive formats
	 */
	'zip'       => 'zip.png',
	'tar'       => 'tar.png',
	'tgz'       => 'tgz.png',
	'bz2'       => 'bzip.png',

	/*
	 * Common source files
	 */
	'h'         => 'h.png',
	'c'         => 'c.png',
	'cpp'       => 'cpp.png',
	'pas'       => 'pas.png',
	'jav'       => 'java.png',
	'java'      => 'java.png',
	'php'       => 'php.png',
	'pl'        => 'pl.png',
	'js'        => 'js.png',

	/*
	 * Common web files
	 */
	'htm'       => 'html.png',
	'html'      => 'html.png',
	'xml'       => 'xml.png',
	'xsl'       => 'xsl.png',
	'css'       => 'css.png',
	'php'       => 'php.png',

    ) ;
?>
