{*
 * Tālikā
 *
 * Template - Complete web page
 * =============================================================================
 * The MIT License
 *
 * Copyright (c) 2012 Sanjeev Premi <spremi@ymail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 * =============================================================================
 *}
<!DOCTYPE html
     PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
     "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html
    xmlns    = "http://www.w3.org/1999/xhtml"
    xml:lang = "en"
    lang     = "en">
<head>

{*
 * Meta information
 *}
<meta name="app" content="{$appName} v{$appVersion}" />
<meta name="author" content="{$appAuthName} ({$appAuthMail})" />
<meta name="theme" content="steel" />

{*
 * Title
 *
 *}
<title>{$appTitle}</title>

{*
 * Base
 *}
<base
    href = "{$baseUrl}" />

{*
 * Stylesheet(s)
 *}
<link
    rel     = "stylesheet"
    type    = "text/css"
    href    = "themes/{$theme}/style.css" />

{*
 * Javascript(s)
 *}
<script
    type    = "text/javascript"
    src     = "js/jquery.js">
</script>

<script
    type    = "text/javascript"
    src     = "themes/{$theme}/js/main.js">
</script>
<script type="text/javascript">
{*
 * Extract directory catalog (if available)
 *}
var ArrCatalog = new Array();
{if $ArrCtlg|@count != 0}
{foreach from=$ArrCtlg key=k item=v}
ArrCatalog['_e_{$k}'] = '{$v}';
{/foreach}
{/if}
</script>
{*
 * Fix PNG transparency issue.
 * (Source: http://homepage.ntlworld.com/bobosola/pngtest.htm)
 *}
<!--[if lt IE 7.]>
<script defer
    type    = "text/javascript">
    src     = "themes/{$theme}/js/pngfix.js">
</script>
<![endif]-->
</head>

<body>
<div id="pgContent">
{*
 * Page header
 *}
<div id="pgHead">
{include
  file = 'pgHeader.tpl'}
</div>

{*
 *  Page body
 *}
<div id="dirName">{$curDir|regex_replace:"/\//":" &raquo; "}</div>

{if !empty($DirInfo)}
<div id="dirInfo">
<h2>Description</h2>
{$DirInfo}
</div>	<!-- dirInfo -->
{/if}
<table id="dirList">
  <thead>
    <tr>
      <th>&nbsp;</th>
      <th class="name">Name</th>
      <th class="perms">Permissions</th>
      <th class="mtime">Modified</th>
      <th class="size">Size</th>
    </tr>
  </thead>
  <tbody>

{foreach $ArrDirs as $dir}
    <tr>
      <td class="icon"><img src="themes/{$theme}/image/{$dir.icon}" /></td>
      <td class="name"><a class="elem" href="?d={$dir.link}">{$dir.name}</a></td>
      <td class="perms">{$dir.perms}</td>
      <td class="mtime">{$dir.mtime}</td>
      <td class="size">&nbsp;</td>
    </tr>
{/foreach}

{foreach $ArrFiles as $file}
    <tr>
      <td class="icon"><img src="themes/{$theme}/image/{$file.icon}" /></td>
      <td class="name"><a class="elem" href="?f={$file.link}">{$file.name}</a></td>
      <td class="perms">{$file.perms}</td>
      <td class="mtime">{$file.mtime}</td>
      <td class="size">{$file.size}</td>
    </tr>
{/foreach}
  </tbody>
</table>
{*
 * File information
 *}
<div id="fileInfo" style="display : none;"></div>
{*
 * About
 *}
<div id="pgAbout" style="display : none;">
{include
  file = 'dlgAbout.tpl'}
</div>
{*
 * Page footer
 *}
<div id="pgFoot">
{include
  file = 'pgFooter.tpl'}
</div>
</div>	<!-- pgContent -->

</body>
</html>
