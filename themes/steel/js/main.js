/*
 * taalika/themes/steel/js/include/main.js
 *
 * Few bells-and-whistles for the user interface
 * =============================================================================
 * The MIT License (http://www.opensource.org/licenses/mit-license.php)
 *
 * Copyright (c) 2012 Sanjeev Premi (spremi at ymail.com).
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * =============================================================================
 */


/*
 * Some key codes
 */
var KeyESC	= 27;
var KeyHOME	= 36;

/**
 * Are we showing the 'About' dialog?
 */
var s_about = false;

/**
 * Position the 'About' box in center of the viewport
 */
function CenterAbout()
{
	  var wh = $(window).height();
	  var ww = $(window).width();

	  var dh = $('#pgAbout').height();
	  var dw = $('#pgAbout').width();

	  var l = (((ww - dw) / 2) + $(window).scrollLeft()) + "px";
	  var t = (((wh - dh) / 2) + $(window).scrollTop()) + "px";

	  $('#pgAbout').css('top', t);
	  $('#pgAbout').css('left', l);
}

/* =============================================================================
 * Actions to be performed once the page contents have been loaded
 * =============================================================================
 */
$(document).ready(function() {
	$('#fileInfo').hide();
	$('#pgAbout').hide();

	CenterAbout();

	/*
	 * Center the About box again, if the window is resized
	 */
	$(window).resize(CenterAbout);

	/*
	 * Add handler for key events
	 */
	$(document).keypress(function(event) {
			var key = (event.keyCode ? event.keyCode : event.which);

			if (s_about) {
				if (key == KeyESC) {
					/*
					 * Hide 'About' box
					 */
					$('#pgAbout').fadeOut('medium');
					s_about = false;
				}
			} else {
				if (key == KeyHOME) {
					/*
					 * Show 'About' box
					 */
					$('#pgAbout').fadeIn('medium');
					s_about = true;
				}
			}
		}
	);

	/*
	 * Show information pane (On MouseOver)
	 */
	$('a.elem').mouseover(function() {
			var idx = '_e_' + this.innerHTML;

			if (idx in ArrCatalog) {
				$('#fileInfo').html(ArrCatalog[idx]);
				$('#fileInfo').show();
			}
		}
	);

	/*
	 * Hide information pane (On MouseOut)
	 */
	$('a.elem').mouseout(function() {
				$('#fileInfo').hide();
			}
	);

	/*
	 * Hide 'About' box on click
	 */
	$('#pgAbout').click(function () {
				$('#pgAbout').fadeOut('medium');
				s_about = false;
			}
	);
});
