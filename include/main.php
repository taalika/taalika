<?php
/*
 * taalika/include/main.php
 *
 * Main application logic.
 * =============================================================================
 * The MIT License (http://www.opensource.org/licenses/mit-license.php)
 *
 * Copyright (c) 2012 Sanjeev Premi (spremi at ymail.com).
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * =============================================================================
 */


/*
 * Prevent direct access
 */
defined ('__TAALIKA__')
or
die(
	'<h2>Taalika</h2>' .
	'<p>Direct access to any of the scripts is not allowed.</p>'
);

/**
 * Script argument - name of the directory to view
 */
$GLOBALS['ArgD'] = "";

/**
 * Script argument - name of the file to download
 */
$GLOBALS['ArgF'] = "";

/**
 * Whether current theme contains icons.
 */
$GLOBALS['ThemeHasIcons'] = false;

/**
 * Name of the 'real' directory
 */
$GLOBALS['RealD'] = "";

/**
 * Name of the 'real' file
 */
$GLOBALS['RealF'] = "";

/**
 * Array containing list of sub-directories contained in a directory.
 */
$GLOBALS['ArrDirs'] = array();

/**
 * Array containing list of files contained in a directory.
 */
$GLOBALS['ArrFiles'] = array();

/**
 * Directory specific information.
 */
$GLOBALS['DirInfo'] = "";

/**
 * Array containing additional description about individual elements
 * in the directory.
 */
$GLOBALS['ArrCtlg'] = array();

/**
 * Array containing various errors (if any) encountered during execution
 */
$GLOBALS['Error'] = array();

/**
 * Smarty Object
 */
$GLOBALS['SO'] = null;

/**
 * Status
 */
$Status = SUCCESS;

/*
 * Get script arguments
 */
if (isset($_GET['d'])) {
	$GLOBALS['ArgD'] = $_GET['d'] ;
} elseif (isset($_GET['f'])) {
	$GLOBALS['ArgF'] = $_GET['f'] ;
} else {
	$GLOBALS['ArgD'] = "/";
}

/*
 * Explicitly set timezone.
 *
 * Can be done via php.ini. However, if user doesn't have permissions to
 * change php.ini, explicit setting avoids this warning:
 * PHP Warning:  strftime(): It is not safe to rely on the system's
 * timezone settings. You are *required* to use...
 */
date_default_timezone_set($GLOBALS['Cfg']['timezone']);

/*
 * Check if current theme contains icons.
 */
$GLOBALS['ThemeHasIcons'] = themeHasIcons($GLOBALS['Cfg']['theme']);

/*
 * Initialize Smarty Template Engine
 */
$Status = initSmarty ();

if ($Status == SUCCESS) {
	$GLOBALS['SO']->assign('appName',	APP_NAME);
	$GLOBALS['SO']->assign('appVersion',	APP_VERSION);
	$GLOBALS['SO']->assign('appAuthName',	APP_AUTH_NAME);
	$GLOBALS['SO']->assign('appAuthMail',	APP_AUTH_MAIL);

	if (empty($GLOBALS['Cfg']['title']))
		$GLOBALS['SO']->assign('appTitle',	APP_NAME);
	else
		$GLOBALS['SO']->assignByRef('appTitle',	$GLOBALS['Cfg']['title']);

	$GLOBALS['SO']->assignByRef('baseUrl',	$GLOBALS['Cfg']['baseurl']);
	$GLOBALS['SO']->assignByRef('theme',	$GLOBALS['Cfg']['theme']);

	$GLOBALS['SO']->assignByRef('curDir',	$GLOBALS['ArgD']);
	$GLOBALS['SO']->assignByRef('curFile',	$GLOBALS['ArgF']);

	$GLOBALS['SO']->assignByRef('ArrDirs',	$GLOBALS['ArrDirs']);
	$GLOBALS['SO']->assignByRef('ArrFiles',	$GLOBALS['ArrFiles']);
	$GLOBALS['SO']->assignByRef('DirInfo',	$GLOBALS['DirInfo']);
	$GLOBALS['SO']->assignByRef('ArrCtlg',	$GLOBALS['ArrCtlg']);

	$GLOBALS['SO']->assignByRef('err',	$GLOBALS['Error']);
}

/*
 * Read contents of a directory
 */
if ($GLOBALS['ArgD'] !== "") {
	/*
	 * Is it virtual base itself
	 */
	if ($GLOBALS['ArgD'] === "/") {
		$Status = readVirtualDir($GLOBALS['ArrDirs']);
	} else {
		/*
		* Check if the directory exists
		*/
		$valid = isPathValid($GLOBALS['ArgD']);

		if ($valid) {
			$Status = toPhysical(
					$GLOBALS['ArgD'],
					$GLOBALS['RealD']);

			if ($Status == SUCCESS) {
				if (is_dir($GLOBALS['RealD'])) {
					$Status = readPhysicalDir(
							$GLOBALS['RealD'],
							$GLOBALS['ArrDirs'],
							$GLOBALS['ArrFiles']);
				} else {
					$Status = FAILURE ;
				}
			}
		} else {
			$Status = FAILURE ;
		}
	}

	/*
	 * Render the page
	 */
	if ($Status == SUCCESS) {
		header("Content-type: text/html");

		$GLOBALS['SO']->display('pgMain.tpl');
	}
}
/*
 * Download the file
 */
elseif ($GLOBALS['ArgF'] !== "") {
	/*
	 * Check if the file exists
	 */
	$valid = isPathValid($GLOBALS['ArgF']);
	if ($valid) {
		$Status = toPhysical($GLOBALS['ArgF'], $GLOBALS['RealF']);

		if ($Status == SUCCESS)
			$Status = serveFile($GLOBALS['RealF']);
	} else {
		$Status = FAILURE ;
	}
}

/*
 * Show errors (if any)
 */
if ($Status == FAILURE)
	$GLOBALS['SO']->display('pgError.tpl');

?>
