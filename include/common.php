<?php
/*
 * taalika/include/common.php
 *
 * Common functions used in the application.
 * =============================================================================
 * The MIT License (http://www.opensource.org/licenses/mit-license.php)
 *
 * Copyright (c) 2012 Sanjeev Premi (spremi at ymail.com).
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * =============================================================================
 */


/*
 * Prevent direct access
 */
defined ('__TAALIKA__')
or
die(
	'<h2>Taalika</h2>' .
	'<p>Direct access to any of the scripts is not allowed.</p>'
);

/**
 * Function checks if the application is properly installed.
 */
function isInstalled()
{
	$ret = false ;

	if (file_exists('./config/settings.php')) {
		require_once('./config/settings.php');

		if (isset($GLOBALS['Cfg']['installed'])
			&& ($GLOBALS['Cfg']['installed'] == true))
			$ret = true ;
	}

	return $ret ;
}

/**
 * Read contents of the 'virtual' base directory.
 */
function readVirtualDir(&$arrD)
{
	$icon = getIcon('_DIR_');

	foreach ($GLOBALS['VirtualRoot'] as $key => $info) {
		$arrD[] = array(
				'name'  => $key,
				'link'  => $GLOBALS['ArgD'] . $key,
				'perms' => "",
				'mtime' => "",
				'icon'  => $icon
			);

		$GLOBALS['ArrCtlg'][$key] = $info['desc'];
	}

	return SUCCESS;
}


/**
 *  Read contents of the 'physical' directory.
 */
function readPhysicalDir(&$dir, &$arrD, &$arrF)
{
	$ret = SUCCESS;

	$flagInfo = false ;		// Dir contains 'information' file
	$flagCtlg = false ;		// Dir contains 'catalog' file
	$flagStat = false ;		// Dir contains 'statistics' file
	$list     = array();

	/*
	 * Open directory and read contents of the directory.
	 */
	$dh = opendir($dir);

	if ($dh) {
		$icon = getIcon('_UP_');

		$arrD[] = array(
				'name'  => "..",
				'link'  => getParent($GLOBALS['ArgD']),
				'perms' => "",
				'mtime' => "",
				'icon'  => $icon,
			);

		/*
		 * Iterate over each element in the directory
		 */
		while (($e = readdir($dh)) !== false) {
			if (   ($e === ".")
			    || ($e === "..")
			    || (preg_match("/^index\./", $e))) {
			}
			elseif ($e === $GLOBALS['Cfg']['info']) {
				$flagInfo = true ;
			}
			elseif ($e === $GLOBALS['Cfg']['catalog']) {
				$flagCtlg = true ;
			}
			elseif ($e === $GLOBALS['Cfg']['stats']) {
				$flagStat = true ;
			}
			else {
				$list[] = $e ;
			}
		}

	    closedir ($dh);
	} else {
		$ret = FAILURE ;
	}

	/*
	 * Get information about this directory (if available).
	 * TODO
	 */
	if (($ret == SUCCESS)
	    && ($flagInfo == true)) {
	      $f_info = $dir ."/" . $GLOBALS['Cfg']['info'];

	      $GLOBALS['DirInfo'] = file_get_contents($f_info);
	}

	/*
	 * Read contents of the catalog file (if available).
	 *
	 * FILE_IGNORE_NEW_LINES requires an update to php.ini. Hence, it is
	 * not being used.
	 */
	if (($ret == SUCCESS)
	    && ($flagCtlg == true)) {
		$tmp = file($dir . "/" . $GLOBALS['Cfg']['catalog'], FILE_SKIP_EMPTY_LINES);

		foreach ($tmp as $line) {
			list($name, $desc) = preg_split("/\s*\|\s*/", trim($line));

			$GLOBALS['ArrCtlg'][$name] = $desc ;
		}
	}

	/*
	 * Get download statistics (if available).
	 * TODO
	 */
	if (($ret == SUCCESS)
	    && ($flagStat == true)) {
	}

	/*
	 * Populate directory and file arrays
	 */
	if ($ret == SUCCESS) {
		foreach ($list as $e) {
			$item = $dir . "/" . $e ;

			$mtime = getModifTime($item);
			$perms = getPerms($item);

			if (is_dir($item)) {
				$icon = getIcon('_DIR_');

				$arrD[] = array(
						'name'  => $e,
						'link'  => $GLOBALS['ArgD'] . "/" . $e,
						'perms' => $perms,
						'mtime' => $mtime,
						'icon'  => $icon,
					    );
			} else {
				$icon = getIcon($item);
				$size = getSize($item);

				$arrF[] = array(
						'name'  => $e,
						'link'  => $GLOBALS['ArgD'] . "/" . $e,
						'perms' => $perms,
						'mtime' => $mtime,
						'size'  => number_format($size),
						'icon'  => $icon,
					);
			}
		}
	}
    return $ret ;
}


function isPathValid($path)
{
	$ret = SUCCESS;

	if (preg_match("/^\.\./", $path))
		$ret = FAILURE ;

	return $ret ;
}


/**
 *  Get physical path corresponding to the virtual path
 */
function toPhysical(&$vpath, &$ppath)
{
	$ret = SUCCESS;

	if ($vpath === "/") {
		$ppath  = "" ;
	} else {
		/*
		* Extract the 'virtual' base
		*/
		$tmp = explode("/", $vpath);

		if ($tmp [0] === "")
			array_shift($tmp);

		$vbase = array_shift($tmp);

		/*
		 * Do we have a map for this base?
		 */
		if (array_key_exists ($vbase, $GLOBALS['VirtualRoot'])) {
			/*
			 * Get corresponding 'physical' base
			 */
			$ppath = $GLOBALS['VirtualRoot'][$vbase]['real'] ;

			/*
			 * Generate 'physical' path
			 */
			$ppath .= "/" . implode("/", $tmp);
		} else {
			$ret = FAILURE ;
			$ppath  = "" ;
		}
	}

	return $ret ;
}

/**
 *  Get parent for specified directory
 */
function getParent($d)
{
	$parent = "" ;

	if ($d !== "/") {
		$tmp   = explode("/", $d);

		array_pop($tmp);

		$parent = implode("/", $tmp);

		if ($parent === "")
			$parent = "/" ;
	}

	return $parent ;
}


/**
 *  Get the modif time for specified file/ directory
 */
function getModifTime($f)
{
	$time = date("M d Y H:i:s", filemtime($f));

	return $time ;
}

/**
 *  Get *NIX style permissions for specified file/ directory
 */
function getPerms($f)
{
	$perms = fileperms($f);

	/*
	 * Type
	 */
	if (($perms & 0xC000) == 0xC000)	// Socket
		$str = 's' ;
	elseif (($perms & 0xA000) == 0xA000)	// Symbolic link
		$str = 'l' ;
	elseif (($perms & 0x8000) == 0x8000)	// Regular file
		$str = '-' ;
	elseif (($perms & 0x6000) == 0x6000)	// Block device
		$str = 'b' ;
	elseif (($perms & 0x4000) == 0x4000)	// Directory
		$str = 'd' ;
	elseif (($perms & 0x2000) == 0x2000)	// Character device
		$str = 'c' ;
	elseif (($perms & 0x1000) == 0x1000)	// Pipe
		$str = 'p' ;
	else					// Unknown - Something new?
		$str = 'u' ;

	/*
	 * Owner
	 */
	$str .= (($perms & 0x0100) ? 'r' : '-');
	$str .= (($perms & 0x0080) ? 'w' : '-');
	$str .= (($perms & 0x0040) ?
		    (($perms & 0x0800) ? 's' : 'x' ) :
		    (($perms & 0x0800) ? 'S' : '-'));

	/*
	 * Group
	 */
	$str .= (($perms & 0x0020) ? 'r' : '-');
	$str .= (($perms & 0x0010) ? 'w' : '-');
	$str .= (($perms & 0x0008) ?
		    (($perms & 0x0400) ? 's' : 'x' ) :
		    (($perms & 0x0400) ? 'S' : '-'));

	/*
	 * World
	 */
	$str .= (($perms & 0x0004) ? 'r' : '-');
	$str .= (($perms & 0x0002) ? 'w' : '-');
	$str .= (($perms & 0x0001) ?
		    (($perms & 0x0200) ? 't' : 'x' ) :
		    (($perms & 0x0200) ? 'T' : '-'));

	return $str ;
}


/**
 *  Get size for specified file
 *
 *  Takes care of the file sizes >2GB (PHP's integer type is signed).
 */
function getSize($f)
{
	$size = sprintf("%u", filesize($f));

	return $size ;
}

/**
 * Function checks if specified theme contains icons.
 */
function themeHasIcons(&$theme)
{
	$ret = false;

	$icondef = APP_BASE . '/themes/' . $GLOBALS['Cfg']['theme'] . '/icons.php';

	if (file_exists($icondef)) {
		require_once($icondef);

		if (is_array($GLOBALS['Icons']))
			$ret = true;
		else
			$ret = false;
	} else {
		$ret = false;
	}

	return $ret ;
}

/**
 *  Get the icon associated to the file (based on file extension)
 */
function getIcon($f)
{
	$icon = "";
	$fextn = "" ;

	if ($f == '_DIR_') {
		$fextn = "_DIR_" ;
	} elseif ($f == '_UP_') {
		$fextn = "_UP_" ;
	} else {
		$tmp = explode(".", basename($f));

		if (count($tmp) > 1) {
			$fextn = end($tmp);
		}
	}

	if ($GLOBALS['ThemeHasIcons']) {
		$icon = 'themes/' . $GLOBALS['Cfg']['theme'] . '/images/';

		if (array_key_exists ($fextn, $GLOBALS['Icons'])) {
			$icon .= $GLOBALS['Icons'][$fextn];
		} else {
			$icon .= $GLOBALS['Icons']['default'];
		}
	}

	return $icon ;
}

/**
 * Serve the file
 */
function serveFile($f)
{
	$ret = SUCCESS;

	header('Content-Description: File Transfer');
	header('Content-Type: application/octet-stream');
	header('Content-Disposition: attachment; filename="'. basename($f) . '";');
	header('Content-Transfer-Encoding: binary');
	header('Pragma: public');
	header('Content-Length: '. filesize($f));

	/*
	 * Stop output buffering if it's already started
	 */
	if (ob_get_level())
		ob_end_clean();

	flush();

	$ret = readfile($f);

	if (($ret === false) || ($ret != filesize($f)))
	      $ret = FAILURE;

	return $ret;
}

/**
 *  Initialize Smarty template engine
 */
function initSmarty()
{
	$ret = SUCCESS;

	require_once($GLOBALS['Cfg']['smartydir'] . '/Smarty.class.php');

	$GLOBALS['SO'] =& new Smarty();

	if (is_null($GLOBALS['SO'])) {
		$ret = FAILURE ;
	} else {
		$themedir = APP_BASE . '/themes/' . $GLOBALS['Cfg']['theme'] . '/';

		$GLOBALS['SO']->template_dir = $themedir . 'templates' ;
		$GLOBALS['SO']->compile_dir  = $themedir . 'templates_c' ;
		$GLOBALS['SO']->config_dir   = $themedir . 'configs' ;
		$GLOBALS['SO']->cache_dir    = $themedir . 'cache' ;
	}

	return $ret ;
}
?>
