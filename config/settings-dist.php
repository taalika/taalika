<?php
/*
 * taalika/config/settings-dist.php
 *
 * Defines configuration settings (as distributed) used in the application.
 * This file can be used as template to create local settings.
 * =============================================================================
 * The MIT License (http://www.opensource.org/licenses/mit-license.php)
 *
 * Copyright (c) 2012 Sanjeev Premi (spremi at ymail.com).
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * =============================================================================
 */


/*
 * Prevent direct access
 */
defined ('__TAALIKA__')
or
die(
	'<h2>Taalika</h2>' .
	'<p>Direct access to any of the scripts is not allowed.</p>'
);

// =============================================================================
// Base configuration
// =============================================================================
$GLOBALS['Cfg'] = array(

	/*
	 * Is the installation complete?
	 */
	'installed'	=> false,

	/*
	 * Title of application
	 * If the string is empty, default application name is used.
	 */
	'title'		=> "",

	/*
	 * Base URL on the web server
	 * Must end with a "/"
	 */
	'baseurl'	=> "http://localhost/taalika/",

	/*
	 * Timezone
	 */
	'timezone'	=> "Etc/GMT",

	/*
	 * Theme to be used
	 */
	'theme'		=> "default",

	/*
	 * Location of Smarty Template Engine
	 * (Smarty is not distributed along with this package)
	 *
	 * This can be an absolute path to an existing Smarty installation.
	 */
	'smartydir'	=> "./Smarty-3.1.11/libs",

	/*
	 * Name of information file
	 * Contains generic description of the contents in the directory.
	 */
	'info'		=> ".info",

	/*
	 * Name of catalog file
	 * Contains additional description about all/ selected elements
	 * in the directory.
	 */
	'catalog'	=> ".catalog",

	/*
	 * Name of statistics file
	 * Contains download statistics for each item in the directory.
	 */
	'stats'		=> ".statistics",
);


// =============================================================================
// Virtual base directory
//
// Note: SELinux doesn't allow browsing of home directories via httpd server.
//       You may need to enable additional SELinux booleans for using this
//       script.
// =============================================================================

$GLOBALS['VirtualRoot'] = array(
	'demo1'		=> array(
				'real'  =>  "/home/developer/Public/demo1",
				'desc'  =>  "Demo directory 1"
			),
	'demo2'		=> array(
				'real'  =>  "/home/developer/Public/demo2",
				'desc'  =>  "Demo directory 2"
			),
);

?>
